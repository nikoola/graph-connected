# GRAPH-CONNECTED
Check if 2 routes are connected

# How to install
Required:
* python
* pip

pip install -r req.txt

# How to use
```shell script
python main.py -h 
```
Gets you:
```shell script
optional arguments:
  -h, --help            show this help message and exit
  -a, --all_image       Get image of all nodes in graph.
  -f FILE, --file FILE  Image File name
  -d, --draw            Draw image of search.
  -g GROUP, --group GROUP
                        Select what group to work on
  -u UNINFORMED [UNINFORMED ...], --uninformed UNINFORMED [UNINFORMED ...]
                        Do an uninformed search [from] -> [to]
  -i INFORMED [INFORMED ...], --informed INFORMED [INFORMED ...]
                        Do an informed search[from] -> [to]
  -t TEST, --test TEST  Insert a number of test you want to be done on
                        searches
```
Example:
```shell script
python main.py -a -f Slika.png
python main.py -t 100
```
# Made by:
 * @nikoola Nikola Smiljanic
