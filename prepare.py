import json
import math

import requests
import attr

from collections import defaultdict
from os import path

from json_loader import PythonObjectEncoder, as_python_object


@attr.s
class Graph:
    data = attr.ib(type=defaultdict(dict), default=defaultdict(dict))
    name = attr.ib(type=str, default='airline_roots')
    group_number = attr.ib(type=int, default=0)

    @classmethod
    def group_nods(cls, flight_data, air_data):
        """ Creates a Graph and makes groups of connected elements
        :param flight_data: src and all its destinations
        :return: Graph class
        """
        visited = set()
        data = {}
        queue = []
        group = 1
        all_stops = list(flight_data.keys())
        while all_stops:
            queue.append(all_stops[0])
            while queue:
                node = queue[0]
                if node in visited:
                    queue.pop(0)
                    continue
                visited.add(node)
                all_stops.remove(node)

                data.update({
                    node: {
                        'neighbours': flight_data[node],
                        'group': group
                    }
                })
                data[node].update(air_data.get(node, {'error': True}))
                queue.extend(list(flight_data[node]))
                queue.pop(0)
            group += 1

        return cls(data=data, name='airline_roots', group_number=group)

    def get_all_stops(self):
        return self.data.keys()

    def get_neighbors_for_node(self, node):
        return self.data[node]['neighbours']

    def get_routes_for_node(self, node):
        return [(node, dst) for dst in self.data[node]['neighbours']]

    def get_group_nodes(self, num):
        if num > self.group_number:
            return []
        else:
            return [item[0] for item in self.data.items() if item[1]['group'] == num]

    def heuristic(self, src: str, dst: str) -> float:
        """ Returns the distance between 2 airports.
        In case any of the airports dont have location information returns 500
        """
        if any([self.data[_].get('error') for _ in [src, dst]]):
            return 500
        x_long, x_lat = self.data[src]['longitude'], self.data[src]['latitude']
        y_long, y_lat = self.data[dst]['longitude'], self.data[dst]['latitude']

        return math.sqrt(((y_long - x_long) ** 2) + ((y_lat - x_lat) ** 2))


def prepare_data() -> Graph:
    flight_graph = Graph()
    air_data = airport_data()
    if path.exists('data/prepare.json'):
        return import_data()
    else:
        return download_data(flight_graph, air_data)


def download_data(flight_graph: Graph, air_data: dict) -> Graph:
    """ get data from openflights.org and create a graph from it
    data from openflights looks like this:
        - 2B,410,AER,2965,KZN,2990,,0,CR2
    from this data only AER - src and KZN dst is used
    :return: Graph
    """
    response = requests.get("https://raw.githubusercontent.com/jpatokal/openflights/master/data/routes.dat")
    flight_routes = defaultdict(set)
    for route in response.text.split('\n'):
        data = route.split(',')
        if len(data) < 4:
            # skip data that has no dst
            continue
        src, dst = data[2], data[4]

        # add src - dst and dst - src path to graph
        flight_routes[src].add(dst)
        flight_routes[dst].add(src)

    graph = flight_graph.group_nods(flight_routes, air_data)
    with open('data/prepare.json', 'w+') as f:
        f.write(json.dumps(graph, cls=PythonObjectEncoder))
    return graph


def import_data() -> Graph:
    with open('data/prepare.json', 'r') as f:
        graph = json.loads(f.read(), object_hook=as_python_object)

    return graph


def airport_data() -> dict:
    response = requests.get('https://raw.githubusercontent.com/jpatokal/openflights/master/data/airports.dat')
    data = {}
    for airport in response.text.split('\n'):
        airport = airport.replace('"', '')
        if not airport:
            continue
        num, full_name, _, country, iata, _, latitude, longitude, altitude, *_ = airport.split(',')
        try:
            float(latitude)
        except ValueError:
            # sometimes countries have an " in name so we add another _ after name
            num, full_name, _, _, country, iata, _, latitude, longitude, altitude, *_ = airport.split(',')
        data.update({
            iata: {
                'name': full_name,
                'country': country,
                'iata': iata,
                'longitude': float(longitude),
                'latitude': float(latitude),
                'altitude': float(altitude)
            }
        })
    return data



if __name__ == '__main__':
    prepare_data()
