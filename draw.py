import networkx as nx
import matplotlib.pyplot as plt


def draw_graph(graph, file_name, group=False, with_labels=False):
    """ Create a picture from graph
    :return: picture.png
    """
    G = nx.Graph()
    all_stops = graph.get_group_nodes(group) if group else graph.get_all_stops()
    for node in all_stops:
        G.add_edges_from(graph.get_routes_for_node(node), color='blue')

    # draw the image
    edges, colors = zip(*nx.get_edge_attributes(G, 'color').items())
    nx.draw(G, edgelist=edges, edge_color=colors, with_labels=with_labels)
    plt.savefig(file_name)
    plt.close()
