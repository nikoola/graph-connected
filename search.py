import random
from datetime import datetime

from prepare import Graph


def time_counter(func):
    """ Records the time spent needed to finish the function.
    :return data from func and time spent doing it
    """

    def function_wrapper(*args, **kwargs):
        start_time = datetime.now()
        data = func(*args, **kwargs)
        end_time = datetime.now()
        return data, (start_time, end_time, end_time - start_time)

    return function_wrapper


def generate_graph_data(data, time):
    path, visited = data
    time_start, time_end, time_spent = time
    return {
        'path': path,
        'path_len': len(path),
        'visited': visited,
        'time_start': time_start,
        'time_end': time_end,
        'time_spent': time_spent
    }


def uninformed(graph: Graph, src: str, dst: str) -> dict:
    """ Run all uninformed searches and return data in dict.
    :return data: dict holding search information
    """
    search_functions = [(bfs, 'BFS'), (dfs, 'DFS'), (rnd, 'RND'), (rnd_improved, 'RND-I')]
    data = [(name, f(graph, src, dst),) for f, name in search_functions]
    return {name: generate_graph_data(*d) for name, d in data}


def informed(graph: Graph, src: str, dst: str, mul=2) -> dict:
    data = a_star(graph, src, dst, mul)
    return {'A_STAR': generate_graph_data(*data)}


def generate_path(parent: dict, src: str, end: str) -> list:
    """ Backtracking for BFS algorithm to get the path.
    """
    path = [end]
    while path[-1] != src:
        path.append(parent[path[-1]])
    path.reverse()
    return path


@time_counter
def bfs(graph, src, dst) -> tuple:
    """ BFS (Breadth-first search) with path saving in dict.
    :return: path and number of visited nodes
    """
    parent = {}
    queue = [src]
    visited = set()
    while queue:
        node = queue.pop(0)
        visited.add(node)
        if node == dst:
            return generate_path(parent, src, dst), len(visited)
        for adjacent in graph.get_neighbors_for_node(node):
            if adjacent in visited:
                continue
            visited.add(adjacent)
            if adjacent not in queue:
                parent[adjacent] = node
                queue.append(adjacent)
            if adjacent == dst:
                parent[adjacent] = node
                return generate_path(parent, src, dst), len(visited)
    return [], len(visited)


@time_counter
def dfs(graph: Graph, src: str, dst: str) -> tuple:
    """ DFS (Breadth-first search) with path saving in list.
    :return: path and number of visited nodes
    """
    queue = [src]
    visited = set()
    while queue:
        node = queue[-1]
        for adjacent in graph.get_neighbors_for_node(node):
            if adjacent in visited:
                continue
            if adjacent == dst:
                queue.append(adjacent)
                visited.add(adjacent)
                return queue, len(visited)
            queue.append(adjacent)
            visited.add(adjacent)
            break
        else:
            queue.pop(-1)

    return [], len(visited)


@time_counter
def rnd(graph: Graph, src: str, dst: str) -> tuple:
    """Selects a random neighbour from the list until if finds dst node
    :return: path and number of visited nodes
    """
    path = []
    select = [src]
    visited = set()
    while select:
        node = random.choice(select)
        visited.add(node)
        neighbours = graph.get_neighbors_for_node(node)
        if neighbours in visited:
            select.remove(node)
            continue
        if node == dst:
            path.append(dst)
            return path, len(path)
        visited.add(node)
        path.append(node)
        select.extend(neighbours - visited)

    return [], len(visited)


@time_counter
def rnd_improved(graph: Graph, src: str, dst: str) -> tuple:
    """Selects a random neighbour from the list until if finds a node that has the endpoint in neighbours
    :return: path and number of visited nodes
    """
    path = []
    select = set()
    select.add(src)
    while select:
        node = random.choice(list(select))
        neighbours = graph.get_neighbors_for_node(node)
        if neighbours in path:
            select.pop(-1)
            continue
        if dst in neighbours:
            path.append(node)
            path.append(dst)
            return path, len(path)
        path.append(node)
        select.update(neighbours)

    return [], len(path)


@time_counter
def a_star(graph: Graph, src: str, dst: str, mul: int, weight: int = 1) -> tuple:
    """ A-Star with node distance as heuristic
    :return: path and number of visited nodes
    """
    open_list = [src]
    closed_list = []
    g = {src: 0}
    parent = {src: src}
    number_of_nodes_checked = 0
    while len(open_list) > 0:
        number_of_nodes_checked += 1
        n = None

        # chose the smallest value
        for v in open_list:
            if not n or g[n] + graph.heuristic(n, dst) * mul > g[v] + graph.heuristic(v, dst) * mul:
                n = v

        if n == dst:
            path = []
            while parent[n] != n:
                path.append(n)
                n = parent[n]
            path.append(src)
            path.reverse()
            return path, number_of_nodes_checked

        for path in graph.get_neighbors_for_node(n):
            if path not in open_list and path not in closed_list:
                open_list.append(path)
                g[path] = weight + g[n]
                parent[path] = n
            else:
                if g[path] > g[n] + weight:
                    g[path] = g[n] + weight
                    parent[path] = parent[n]
                    if g[path] in closed_list:
                        closed_list.remove(path)
                        open_list.append(path)

        open_list.remove(n)
        closed_list.append(n)

    return [], -1
