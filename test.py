import datetime
import random

from prepare import Graph
from search import uninformed, informed


def average_data(data: list, bfs_total: int, u_inf: bool = True) -> dict:
    search = ['BFS', 'DFS', 'RND', 'RND-I'] if u_inf else ['A_STAR']
    average_dict = {}

    for item in search:
        item_data = [_[item] for _ in data]
        average_dict.update({
            item: {
                'longest_time': max(item_data, key=lambda i: i['time_spent'])['time_spent'],
                'shortest_time': min(item_data, key=lambda i: i['time_spent'])['time_spent'],
                'total_time': sum([item['time_spent'] for item in item_data], datetime.timedelta()),
                'total_visited': sum([item['visited'] for item in item_data]),
                'total_missed_path': sum([item['path_len'] for item in item_data]) - bfs_total,
                'total_path': sum([item['path_len'] for item in item_data])
            }
        })

    return average_dict


def test_all_searches(graph: Graph, num: int) -> dict:
    """ Runes all the test for num time and returns results of the given tests
    src, and dst is taken randomly from the graph
    """
    all_nodes = graph.get_group_nodes(1)
    uninformed_all = []
    informed_all = []
    for _ in range(num):
        src, dst = random.sample(all_nodes, 2)
        print(f'{_} : {src} {dst}')
        uninformed_data = uninformed(graph, src, dst)
        informed_data = informed(graph, src, dst)
        uninformed_all.append(uninformed_data)
        informed_all.append(informed_data)

    bfs_total = sum([item['path_len'] for item in [d.get('BFS') for d in uninformed_all]])
    data = average_data(uninformed_all, bfs_total)
    data.update(average_data(informed_all, bfs_total, u_inf=False))
    return data
