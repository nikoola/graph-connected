import sys
import argparse

from datetime import datetime
from json import dumps

from draw import draw_graph
from prepare import prepare_data
from search import uninformed, informed
from json_loader import time_converter
from test import test_all_searches

graph = prepare_data()

# prepare parser for file
parser = argparse.ArgumentParser('Check if the path exists')
parser.add_argument('-a', '--all_image', help='Get image of all nodes in graph.', action='store_true')
parser.add_argument('-f', '--file', type=str, required=False, default='picture.png', help="Image File name")
parser.add_argument('-d', '--draw', help='Draw image of search.', action='store_true')
parser.add_argument('-g', '--group', help="Select what group to work on", type=int, required=False, default=0)
parser.add_argument('-u', '--uninformed', help="Do an uninformed search [from] -> [to] ", nargs='+', type=str)
parser.add_argument('-i', '--informed', help='Do an informed search[from] -> [to] ', nargs='+', type=str)
parser.add_argument('-t', '--test', help='Insert a number of test you want to be done on searches', required=False,
                    default=0, type=int)
args = parser.parse_args()


def check_nodes_in_group(nodes):
    if len(nodes) != 2:
        print("Wrong number of params sent. 2 needed [src] and [dst]")
        sys.exit(-1)
    elif graph.data.get(nodes[0]) is None or graph.data.get(nodes[1]) is None:
        print(f"One or both nodes not found in graph. Used: {nodes[0]}, {nodes[0]}")
        sys.exit(-1)
    elif graph.data[nodes[0]]['group'] != graph.data[nodes[1]]['group']:
        print(f"SRC: {nodes[0]} and DST{nodes[0]} in different groups")
        sys.exit(-1)
    elif nodes[0] == nodes[1]:
        print(f'src cant be same as dst')
        sys.exit(-1)


if __name__ == '__main__':
    print(f'Start: {datetime.now()}')
    if args.all_image:
        draw_graph(graph=graph, file_name=args.file)
        print(f'Finish: {datetime.now()}')
        sys.exit(0)
    if args.group:
        draw_graph(graph=graph, file_name=args.file, group=args.group, with_labels=True)
        print(f'Finish: {datetime.now()}')
        sys.exit(0)
    if args.uninformed:
        check_nodes_in_group(args.uninformed)
        path_data = uninformed(graph, args.uninformed[0], args.uninformed[1])
        print(f"DATA:\n{dumps(path_data, sort_keys=True, indent=2, default=time_converter)}")
    if args.informed:
        check_nodes_in_group(args.informed)
        path_data = informed(graph, args.informed[0], args.informed[1])
        print(f"DATA:\n{dumps(path_data, sort_keys=True, indent=2, default=time_converter)}")
    if args.test:
        data = test_all_searches(graph, args.test)
        print(f"DATA:\n{dumps(data, sort_keys=True, indent=2, default=time_converter)}")

    print(f'End: {datetime.now()}')
    sys.exit(2)
