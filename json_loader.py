import pickle

from base64 import b64encode, b64decode
from datetime import datetime, timedelta
from json import JSONEncoder


class PythonObjectEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, (list, dict, str, int, float, bool, type(None))):
            return super().default(obj)
        return {'_python_object': b64encode(pickle.dumps(obj)).decode('utf-8')}


def as_python_object(dct):
    if '_python_object' in dct:
        return pickle.loads(b64decode(dct['_python_object'].encode('utf-8')))
    return dct


def time_converter(o):
    if isinstance(o, (datetime, timedelta)):
        return o.__str__()
